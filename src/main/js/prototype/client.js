'use strict';

$(document).ready(function () {

  var plan = 'plan#1';
  // objects with id and free (true, false)
  var initialData = [];

  var initData = function () {
    var http = new XMLHttpRequest();
    var url = 'http://www.yandex.ru';
    var params = 'plan=' + plan;
    http.open('GET', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function () {//Call a function when the state changes.
      if (http.readyState == 4 && http.status == 200) {
        initialData = JSON.parse(http.responseText);
      }
    }
    http.send(params);
  }

  // temporary for tests
  initialData.push({
    id: 1,
    url: 'yandex.ru',
    coords: [3,47,45,12,105,7,140,60,120,125,12,90],
    free: true
  })
  initialData.push({
    id: 2,
    url: 'yandex.ru',
    coords: [347,57,453,64,458,136,356,127],
    free: false
  })

  // Model = room id, free (true, false), coordinates (array)
  function createElement(model) {
    var id = model.id;
    var coords = model.coords;
    var url = model.url;

    var element = document.createElement("area");
    element.setAttribute("shape", "poly");
    element.setAttribute("coords", coords.join(","));
    element.setAttribute("href", url);
    element.classList.add("mapping");
    if (model.free) {
      element.dataset.maphilight = '{"strokeColor":"0000ff","strokeWidth":5,"fillColor":"ff0000","fillOpacity":0.6}';
    }

    return element;
  }

  for (var i = 0; i<initialData.length; i++) {
    document.getElementsByName('area-link')[0].appendChild(createElement(initialData[i]));
  }
});
